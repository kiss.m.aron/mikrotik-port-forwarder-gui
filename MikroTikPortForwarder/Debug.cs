﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Kisem.Debug
{
    static class Debug
    {
        #region Private variables
        private static bool enabled = true;
        private static bool messageBoxes = false;
        private static bool timeStamp = false;
        private static bool logToFile = true;
        private static string logFile = "portforwarder.log";
        #endregion
        #region Properties
        public static bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }
        public static bool MessageBoxes
        {
            get
            {
                return messageBoxes;
            }
            set
            {
                if (value == true)
                {
                    Info("MessageBoxes are enabled!");
                    Warn("We suggest you to turn them off while you are debugging.");
                }
                else
                {
                    Info("MessageBoxes are disabled.");
                }
                messageBoxes = value;
            }
        }
        public static bool TimeStamp
        {
            get
            {
                return timeStamp;
            }
            set
            {
                if (value == true)
                {
                    Info("Timestamps are enabled.");
                }
                else
                {
                    Info("Timestamps are disabled.");
                }
                timeStamp = value;
            }
        }
        public static bool LogToFile
        {
            get
            {
                return logToFile;
            }
            set
            {
                logToFile = value;
            }
        }
        public static string LogFile
        {
            get
            {
                return logFile;
            }
            set
            {
                logFile = value;
            }
        }
        #endregion
        #region Logging functions
        public static void Log(object message, bool breakLine = true)
        {
            Print(message, "log", breakLine);
        }
        public static void Info(object message, bool breakLine = true)
        {
            Print(message, "info", breakLine);
        }
        public static void Warn(object message, bool breakLine = true)
        {
            Print(message, "warn", breakLine);
        }
        public static void Error(object message, bool breakLine = true)
        {
            Print(message, "error", breakLine);

            if (messageBoxes)
            {
                MessageBox.Show(message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static void Success(object message = null, bool breakLine = true)
        {
            if (message != null)
            {
                Print(message, "success", breakLine);
            }
            else
            {
                Print("", "done", breakLine);
            }
        }
        #endregion
        #region Formatting functions
        public static void NewLine(byte count = 1, string pattern = null)
        {
            if (!enabled) return;

            for (byte i = 0; i < count; i++)
            {
                if (pattern != null)
                {
                    if (i == 0 || i == count - 1)
                    {
                        for (byte j = 0; j < 80 / pattern.Length; j++)
                        {
                            Console.Write(pattern);
                        }
                    }
                }
                Console.WriteLine();
            }
        }
        public static void NewLine(string pattern)
        {
            if (!enabled) return;
            
            for (byte j = 0; j < 80 / pattern.Length; j++)
            {
                Console.Write(pattern);
            }
            Console.WriteLine();
        }
        #endregion
        #region Print functions
        private static void Print(object message, string prefix = null, bool breakLine = true)
        {
            if (!enabled) return;

            string msg = null;
            
            prefix = prefix.ToUpper();
            

            if (prefix != null)
            {
                for (byte i = 0; i < 7 - prefix.Length; i++)
                {
                    msg += " ";
                    //Console.Write(" ");
                }

                msg += "[" + prefix + "] ";
                //Console.Write("[" + prefix + "] ");

                if (timeStamp && prefix != "DONE")
                {
                    msg += "[" + DateTime.Now.ToString("HH:mm:ss") + "] ";
                    //Console.Write("[" + DateTime.Now.ToString("HH:mm:ss") + "] ");
                }
            }

            msg += message;
            //Console.Write(message);

            ConsoleColor color;
            switch (prefix)
            {
                case "INFO":
                    color = ConsoleColor.Cyan;
                    break;
                case "WARN":
                    color = ConsoleColor.Yellow;
                    break;
                case "ERROR":
                    color = ConsoleColor.Red;
                    break;
                case "SUCCESS":
                    color = ConsoleColor.Green;
                    break;
                case "DONE":
                    color = ConsoleColor.Green;
                    break;

                default:
                    color = ConsoleColor.Gray;
                    break;
            }

            if (breakLine)
            {
                msg += "\n";
                //Console.WriteLine();
            }

            Console.ForegroundColor = color;
            Console.Write(msg);
            Console.ResetColor();

            if (logToFile)
            {
                msg = "[" + DateTime.Now.ToString("HH:mm:ss:fff") + "] " + msg;
                File.AppendAllText(logFile, msg + Environment.NewLine);
            }
        }
        #endregion
    }
}
