﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kisem.Debug;

namespace MikroTikPortForwarder
{
    public class Router
    {
        private string ip = "192.168.1.1",
                       username = "admin", 
                       password = "";
        private int port = 22;

        public string IP
        {
            get
            {
                return ip;
            }
            set
            {
                ip = value;
                Debug.Info("Router IP address set to " + ip);
            }
        }
        public int Port
        {
            get
            {
                return port;
            }
            set
            {
                port = value;
                Debug.Info("SSH port set to " + port);
            }
        }
        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
                Debug.Info("SSH username set to " + username);
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                Debug.Info("SSH password set to " + password);
            }
        }


        public Router()
        {
        }
        public Router(string IP)
        {
            ip = IP;
        }
        public Router(string IP, string Password)
        {
            ip = IP;
            password = Password;
        }
        public Router(string IP, string Username, string Password)
        {
            ip = IP;
            username = Username;
            password = Password;
        }
        public Router(string IP, int Port, string Username, string Password)
        {
            ip = IP;
            port = Port;
            username = Username;
            password = Password;
        }
    }
}
