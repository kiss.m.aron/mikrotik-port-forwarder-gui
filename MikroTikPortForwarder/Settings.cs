﻿using Kisem.Debug;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace MikroTikPortForwarder
{
    public static class Forms
    {
        public static List<Form> list = new List<Form>();

        public static void Update()
        {
            foreach (var form in list)
            {
                Settings.Translate(form);
            }
        }
    }

    public static class Settings
    {
        private static string configFile = "settings.cfg";
        public static string ConfigFile
        {
            get { return configFile; }
        }
        private static string language = "en";
        public static string Language
        {
            get { return language; }
            set
            {
                if (!Languages.Contains(value))
                {
                    Debug.Warn("Language file not found!");
                }
                else
                {
                    language = value;
                    Debug.Info("Language set to " + language);
                }
            }
        }
        private static List<string> languages = new List<string>();
        public static List<string> Languages
        {
            get { return languages; }
            set { languages = value; }
        }

        public static Router Router;
        /*
        public static Router Router
        {
            get { return router; }
            set { router = value; }
        }
        */

        public static void Load()
        {
            Router = new Router();
            if (!File.Exists(ConfigFile))
            {
                Save();
            }

            Router = new Router();
            foreach (string line in File.ReadLines(ConfigFile))
            {
                var parts = line.Split('=');
                if (parts.Length != 2) continue;

                var propertyName = parts[0];
                var propertyValue = parts[1];

                var className = propertyName.Split('.');

                if (className.Length == 2)
                {
                    /*
                    var r = typeof(Settings).GetField(className[0]);
                    r.GetType().GetProperty(className[1]).SetValue(r, propertyValue);
                    */
                    var r = typeof(Settings).GetField(className[0]);
                    Router.GetType().GetProperty(className[1]).SetValue(Router, propertyValue);
                }
                else
                {
                    typeof(Settings).GetProperty(propertyName).SetValue(typeof(Settings), propertyValue);
                }
            }

            DirectoryInfo dirInfo = new DirectoryInfo("lang");
            var files = dirInfo.GetFiles("*.lang");
            foreach (var file in files)
            {
                var lang = file.Name.Replace(file.Extension, "");
                Languages.Add(lang);
            }
        }
        public static void Save()
        {
            string data = null;
            data += "Language=" + Language + Environment.NewLine;
            data += "Router.IP=" + Router.IP + Environment.NewLine;
            data += "Router.Username=" + Router.Username + Environment.NewLine;
            data += "Router.Password=" + Router.Password + Environment.NewLine;
            File.WriteAllText(ConfigFile, data);
        }

        public static void ChangeLanguage(Form context, string language)
        {
            Language = language;
            Translate(context);
        }
        public static void Translate(Form context)
        {
            var filePath = "lang/" + Language + ".lang";
            foreach (string line in File.ReadLines(filePath))
            {
                var parts = line.Split('=');

                if (parts.Length < 2) continue;

                var name = parts[0];
                var value = parts[1];

                if (context.Name == name)
                {
                    context.Text = value;
                }

                if (context.Controls.Find("label_" + name, true).Length != 0)
                {
                    context.Controls["label_" + name].Text = value;
                }

                if (context.Controls.Find("button_" + name, true).Length != 0)
                {
                    context.Controls["button_" + name].Text = value;
                }
            }
        }
    }
}
