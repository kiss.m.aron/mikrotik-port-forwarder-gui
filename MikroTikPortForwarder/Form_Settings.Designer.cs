﻿namespace MikroTikPortForwarder
{
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_PrefRouterIP = new System.Windows.Forms.TextBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.label_RouterIP = new System.Windows.Forms.Label();
            this.label_RouterUsername = new System.Windows.Forms.Label();
            this.textBox_PrefRouterUsername = new System.Windows.Forms.TextBox();
            this.textBox_PrefRouterPassword = new System.Windows.Forms.TextBox();
            this.label_RouterPassword = new System.Windows.Forms.Label();
            this.label_Language = new System.Windows.Forms.Label();
            this.comboBox_Language = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // textBox_PrefRouterIP
            // 
            this.textBox_PrefRouterIP.Location = new System.Drawing.Point(116, 46);
            this.textBox_PrefRouterIP.MaxLength = 11;
            this.textBox_PrefRouterIP.Name = "textBox_PrefRouterIP";
            this.textBox_PrefRouterIP.Size = new System.Drawing.Size(132, 20);
            this.textBox_PrefRouterIP.TabIndex = 0;
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(173, 128);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Save.TabIndex = 1;
            this.button_Save.Text = "Save";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_PrefSave_Click);
            // 
            // label_RouterIP
            // 
            this.label_RouterIP.AutoSize = true;
            this.label_RouterIP.Location = new System.Drawing.Point(12, 49);
            this.label_RouterIP.Name = "label_RouterIP";
            this.label_RouterIP.Size = new System.Drawing.Size(52, 13);
            this.label_RouterIP.TabIndex = 2;
            this.label_RouterIP.Text = "Router IP";
            // 
            // label_RouterUsername
            // 
            this.label_RouterUsername.AutoSize = true;
            this.label_RouterUsername.Location = new System.Drawing.Point(12, 75);
            this.label_RouterUsername.Name = "label_RouterUsername";
            this.label_RouterUsername.Size = new System.Drawing.Size(55, 13);
            this.label_RouterUsername.TabIndex = 3;
            this.label_RouterUsername.Text = "Username";
            // 
            // textBox_PrefRouterUsername
            // 
            this.textBox_PrefRouterUsername.Location = new System.Drawing.Point(116, 72);
            this.textBox_PrefRouterUsername.Name = "textBox_PrefRouterUsername";
            this.textBox_PrefRouterUsername.Size = new System.Drawing.Size(132, 20);
            this.textBox_PrefRouterUsername.TabIndex = 4;
            // 
            // textBox_PrefRouterPassword
            // 
            this.textBox_PrefRouterPassword.Location = new System.Drawing.Point(116, 98);
            this.textBox_PrefRouterPassword.Name = "textBox_PrefRouterPassword";
            this.textBox_PrefRouterPassword.Size = new System.Drawing.Size(132, 20);
            this.textBox_PrefRouterPassword.TabIndex = 6;
            // 
            // label_RouterPassword
            // 
            this.label_RouterPassword.AutoSize = true;
            this.label_RouterPassword.Location = new System.Drawing.Point(12, 101);
            this.label_RouterPassword.Name = "label_RouterPassword";
            this.label_RouterPassword.Size = new System.Drawing.Size(53, 13);
            this.label_RouterPassword.TabIndex = 5;
            this.label_RouterPassword.Text = "Password";
            // 
            // label_Language
            // 
            this.label_Language.AutoSize = true;
            this.label_Language.Location = new System.Drawing.Point(12, 9);
            this.label_Language.Name = "label_Language";
            this.label_Language.Size = new System.Drawing.Size(55, 13);
            this.label_Language.TabIndex = 7;
            this.label_Language.Text = "Language";
            // 
            // comboBox_Language
            // 
            this.comboBox_Language.FormattingEnabled = true;
            this.comboBox_Language.Location = new System.Drawing.Point(116, 13);
            this.comboBox_Language.Name = "comboBox_Language";
            this.comboBox_Language.Size = new System.Drawing.Size(132, 21);
            this.comboBox_Language.TabIndex = 8;
            this.comboBox_Language.Text = "123";
            // 
            // Form_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 163);
            this.Controls.Add(this.comboBox_Language);
            this.Controls.Add(this.label_Language);
            this.Controls.Add(this.textBox_PrefRouterPassword);
            this.Controls.Add(this.label_RouterPassword);
            this.Controls.Add(this.textBox_PrefRouterUsername);
            this.Controls.Add(this.label_RouterUsername);
            this.Controls.Add(this.label_RouterIP);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.textBox_PrefRouterIP);
            this.Name = "Form_Settings";
            this.ShowIcon = false;
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_PrefRouterIP;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Label label_RouterIP;
        private System.Windows.Forms.Label label_RouterUsername;
        private System.Windows.Forms.TextBox textBox_PrefRouterUsername;
        private System.Windows.Forms.TextBox textBox_PrefRouterPassword;
        private System.Windows.Forms.Label label_RouterPassword;
        private System.Windows.Forms.Label label_Language;
        private System.Windows.Forms.ComboBox comboBox_Language;
    }
}