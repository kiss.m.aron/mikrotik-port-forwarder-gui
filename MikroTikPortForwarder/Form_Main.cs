﻿using Renci.SshNet;
using Kisem.Debug;

using System;
using System.Text;
using System.Windows.Forms;

namespace MikroTikPortForwarder
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            Debug.Log("Initializing.");

            InitializeComponent();

            Settings.Load();
            Settings.ChangeLanguage(this, "hu");

            Console.OutputEncoding = Encoding.Unicode;
            Debug.Enabled = true;
            Debug.MessageBoxes = true;

            Debug.Info("Router found on 10.0.0.1");
            Debug.NewLine(3, "=");

            Forms.list.Add(this);
        }

        private void button_SendCommand_Click(object sender, EventArgs e)
        {
            string ip = textBox_IP.Text;
            if (!IsValid(ip))
            {
                Debug.Error("IP Address is not valid! Aborting...");
                return;
            }
            var ipFrags = ip.Split('.');
            string subnet = ipFrags[0] + "." + ipFrags[1] + "." + ipFrags[2] + ".0/24";

            int sourcePort = Convert.ToInt32(textBox_SourcePort.Text);
            if (sourcePort < 1 || sourcePort > 65535)
            {
                Debug.Error("Port is not valid! Aborting...");
                return;
            }

            //int destinationPort = Convert.ToInt32(textBox_DestinationPort.Text);
            //if (destinationPort == 0)
                var destinationPort = sourcePort;

            string comment = textBox_Comment.Text;

            try
            {
                using (var client = new SshClient(Settings.Router.IP, Settings.Router.Username, Settings.Router.Password))
                {
                    Debug.Log("Connecting to the router ("+ Settings.Router.IP +")...", false);
                    client.Connect();
                    Debug.Success();

                    #region old command code
                    /*
                    Debug.Log("Adding first rule...", false);
                    var sshCommand = client.RunCommand("/ip firewall nat add chain=dstnat dst-port="+ port + " action=dst-nat protocol=tcp to-address=" + ip + " to-port=" + port + " dst-address-type=local comment=\""+ comment +" - by PortForwarder\"");
                    if (sshCommand.Result.Length == 0)
                    {
                        Debug.Success();
                    }
                    else
                    {
                        Debug.NewLine();
                        Debug.Error(sshCommand.Result);
                        errors += "First rule: " + sshCommand.Result + "\n";
                    }

                    Debug.Log("Adding second rule...", false);
                    sshCommand = client.RunCommand("/ip firewall nat add chain=srcnat src-address="+ subnet +" dst-address="+ ip +" protocol=tcp dst-port=" + port + " out-interface=bridge-local action=masquerade comment=\"" + comment + " - by PortForwarder\"");
                    if (sshCommand.Result.Length == 0)
                    {
                        Debug.Success();
                    }
                    else
                    {
                        Debug.NewLine();
                        Debug.Error(sshCommand.Result);
                        errors += "Second rule: " + sshCommand.Result + "\n";
                    }
                    */
                    #endregion

                    string protocol = (radioButton_TCP.Checked ? "tcp" : "udp");

                    string rule1 = "/ip firewall nat";
                    rule1 += " add";
                    rule1 += " chain=dstnat";
                    rule1 += " dst-port=" + sourcePort;
                    rule1 += " action=dst-nat";
                    rule1 += " protocol=" + protocol;
                    rule1 += " to-address=" + ip;
                    rule1 += " to-port=" + sourcePort;
                    rule1 += " dst-address-type=local";
                    rule1 += " comment=\"" + comment + " - by " + Settings.Router.Username + "\";";

                    string rule2 = "/ip firewall nat";
                    rule2 += " add";
                    rule2 += " chain=srcnat";
                    rule2 += " src-address=" + subnet;
                    rule2 += " dst-address=" + ip;
                    rule2 += " protocol=" + protocol;
                    rule2 += " dst-port=" + sourcePort;
                    rule2 += " out-interface=bridge-local";
                    rule2 += " action=masquerade";
                    rule2 += " comment=\"" + comment + " - by " + Settings.Router.Username + "\";";

                    string command = rule1 + rule2;
                    
                    Debug.Log("Adding nat rules...", false);
                    var sshCommand = client.RunCommand(command);
                    if (sshCommand.Result.Length == 0)
                    {
                        Debug.Success();
                        MessageBox.Show("Nat rules were added to the router.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        Debug.NewLine();
                        Debug.Error(sshCommand.Result);
                    }
                    Debug.NewLine("=");

                    client.Disconnect();
                }
            }
            catch (Exception ex)
            {
                Debug.NewLine();
                Debug.Error("Couldn't login to the router: " + ex.Message);
            }
        }

        bool IsValid(string ip)
        {
            System.Net.IPAddress address;
            return System.Net.IPAddress.TryParse(ip, out address);
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            Form_Settings p = new Form_Settings();
            p.ShowDialog();
        }

        private void button_openLog_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Debug.LogFile);
        }
    }
}
