﻿namespace MikroTikPortForwarder
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_SourcePort = new System.Windows.Forms.TextBox();
            this.button_AddRule = new System.Windows.Forms.Button();
            this.textBox_IP = new System.Windows.Forms.TextBox();
            this.textBox_Comment = new System.Windows.Forms.TextBox();
            this.label_SourcePort = new System.Windows.Forms.Label();
            this.label_Protocol = new System.Windows.Forms.Label();
            this.label_Comment = new System.Windows.Forms.Label();
            this.button_Settings = new System.Windows.Forms.Button();
            this.radioButton_UDP = new System.Windows.Forms.RadioButton();
            this.radioButton_TCP = new System.Windows.Forms.RadioButton();
            this.label_DestinationPort = new System.Windows.Forms.Label();
            this.textBox_DestinationPort = new System.Windows.Forms.TextBox();
            this.button_Log = new System.Windows.Forms.Button();
            this.label_DestinationIP = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox_SourcePort
            // 
            this.textBox_SourcePort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_SourcePort.Location = new System.Drawing.Point(173, 38);
            this.textBox_SourcePort.Name = "textBox_SourcePort";
            this.textBox_SourcePort.Size = new System.Drawing.Size(139, 20);
            this.textBox_SourcePort.TabIndex = 0;
            this.textBox_SourcePort.Text = "3389";
            // 
            // button_AddRule
            // 
            this.button_AddRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_AddRule.Location = new System.Drawing.Point(198, 167);
            this.button_AddRule.Name = "button_AddRule";
            this.button_AddRule.Size = new System.Drawing.Size(114, 23);
            this.button_AddRule.TabIndex = 1;
            this.button_AddRule.Text = "Add rule!";
            //this.button_AddRule.Text = InputLanguageChanged.;
            this.button_AddRule.UseVisualStyleBackColor = true;
            this.button_AddRule.Click += new System.EventHandler(this.button_SendCommand_Click);
            // 
            // textBox_IP
            // 
            this.textBox_IP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_IP.Location = new System.Drawing.Point(173, 12);
            this.textBox_IP.Name = "textBox_IP";
            this.textBox_IP.Size = new System.Drawing.Size(139, 20);
            this.textBox_IP.TabIndex = 2;
            this.textBox_IP.Text = "10.0.0.20";
            // 
            // textBox_Comment
            // 
            this.textBox_Comment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Comment.Location = new System.Drawing.Point(173, 119);
            this.textBox_Comment.Multiline = true;
            this.textBox_Comment.Name = "textBox_Comment";
            this.textBox_Comment.Size = new System.Drawing.Size(139, 42);
            this.textBox_Comment.TabIndex = 3;
            this.textBox_Comment.Text = "Remote Desktop";
            // 
            // label_SourcePort
            // 
            this.label_SourcePort.AutoSize = true;
            this.label_SourcePort.Location = new System.Drawing.Point(13, 41);
            this.label_SourcePort.Name = "label_SourcePort";
            this.label_SourcePort.Size = new System.Drawing.Size(63, 13);
            this.label_SourcePort.TabIndex = 6;
            this.label_SourcePort.Text = "Source Port";
            // 
            // label_Protocol
            // 
            this.label_Protocol.AutoSize = true;
            this.label_Protocol.Location = new System.Drawing.Point(13, 92);
            this.label_Protocol.Name = "label_Protocol";
            this.label_Protocol.Size = new System.Drawing.Size(46, 13);
            this.label_Protocol.TabIndex = 8;
            this.label_Protocol.Text = "Protocol";
            // 
            // label_Comment
            // 
            this.label_Comment.AutoSize = true;
            this.label_Comment.Location = new System.Drawing.Point(13, 122);
            this.label_Comment.Name = "label_Comment";
            this.label_Comment.Size = new System.Drawing.Size(51, 13);
            this.label_Comment.TabIndex = 9;
            this.label_Comment.Text = "Comment";
            // 
            // button_Settings
            // 
            this.button_Settings.Location = new System.Drawing.Point(12, 167);
            this.button_Settings.Name = "button_Settings";
            this.button_Settings.Size = new System.Drawing.Size(74, 23);
            this.button_Settings.TabIndex = 10;
            this.button_Settings.Text = "Settings";
            this.button_Settings.UseVisualStyleBackColor = true;
            this.button_Settings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // radioButton_UDP
            // 
            this.radioButton_UDP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton_UDP.AutoSize = true;
            this.radioButton_UDP.Location = new System.Drawing.Point(264, 90);
            this.radioButton_UDP.Name = "radioButton_UDP";
            this.radioButton_UDP.Size = new System.Drawing.Size(48, 17);
            this.radioButton_UDP.TabIndex = 11;
            this.radioButton_UDP.Text = "UDP";
            this.radioButton_UDP.UseVisualStyleBackColor = true;
            // 
            // radioButton_TCP
            // 
            this.radioButton_TCP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton_TCP.AutoSize = true;
            this.radioButton_TCP.Checked = true;
            this.radioButton_TCP.Location = new System.Drawing.Point(198, 90);
            this.radioButton_TCP.Name = "radioButton_TCP";
            this.radioButton_TCP.Size = new System.Drawing.Size(46, 17);
            this.radioButton_TCP.TabIndex = 12;
            this.radioButton_TCP.TabStop = true;
            this.radioButton_TCP.Text = "TCP";
            this.radioButton_TCP.UseVisualStyleBackColor = true;
            // 
            // label_DestinationPort
            // 
            this.label_DestinationPort.AutoSize = true;
            this.label_DestinationPort.Location = new System.Drawing.Point(13, 67);
            this.label_DestinationPort.Name = "label_DestinationPort";
            this.label_DestinationPort.Size = new System.Drawing.Size(82, 13);
            this.label_DestinationPort.TabIndex = 14;
            this.label_DestinationPort.Text = "Destination Port";
            // 
            // textBox_DestinationPort
            // 
            this.textBox_DestinationPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_DestinationPort.Location = new System.Drawing.Point(173, 64);
            this.textBox_DestinationPort.Name = "textBox_DestinationPort";
            this.textBox_DestinationPort.Size = new System.Drawing.Size(139, 20);
            this.textBox_DestinationPort.TabIndex = 13;
            // 
            // button_Log
            // 
            this.button_Log.Location = new System.Drawing.Point(92, 166);
            this.button_Log.Name = "button_Log";
            this.button_Log.Size = new System.Drawing.Size(52, 23);
            this.button_Log.TabIndex = 15;
            this.button_Log.Text = "Log";
            this.button_Log.UseVisualStyleBackColor = true;
            this.button_Log.Click += new System.EventHandler(this.button_openLog_Click);
            // 
            // label_DestinationIP
            // 
            this.label_DestinationIP.AutoSize = true;
            this.label_DestinationIP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_DestinationIP.Location = new System.Drawing.Point(13, 15);
            this.label_DestinationIP.Name = "label_DestinationIP";
            this.label_DestinationIP.Size = new System.Drawing.Size(73, 13);
            this.label_DestinationIP.TabIndex = 16;
            this.label_DestinationIP.Text = "Destination IP";
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 201);
            this.Controls.Add(this.label_DestinationIP);
            this.Controls.Add(this.button_Log);
            this.Controls.Add(this.label_DestinationPort);
            this.Controls.Add(this.textBox_DestinationPort);
            this.Controls.Add(this.radioButton_TCP);
            this.Controls.Add(this.radioButton_UDP);
            this.Controls.Add(this.button_Settings);
            this.Controls.Add(this.label_Comment);
            this.Controls.Add(this.label_Protocol);
            this.Controls.Add(this.label_SourcePort);
            this.Controls.Add(this.textBox_Comment);
            this.Controls.Add(this.textBox_IP);
            this.Controls.Add(this.button_AddRule);
            this.Controls.Add(this.textBox_SourcePort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form_Main";
            this.Text = "Mikrotik Port Forwarder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_IP;
        private System.Windows.Forms.TextBox textBox_SourcePort;
        private System.Windows.Forms.TextBox textBox_DestinationPort;
        private System.Windows.Forms.TextBox textBox_Comment;
        private System.Windows.Forms.Label label_DestinationIP;
        private System.Windows.Forms.Label label_SourcePort;
        private System.Windows.Forms.Label label_DestinationPort;
        private System.Windows.Forms.Label label_Protocol;
        private System.Windows.Forms.Label label_Comment;
        private System.Windows.Forms.Button button_Log;
        private System.Windows.Forms.Button button_Settings;
        private System.Windows.Forms.Button button_AddRule;
        private System.Windows.Forms.RadioButton radioButton_UDP;
        private System.Windows.Forms.RadioButton radioButton_TCP;
    }
}

