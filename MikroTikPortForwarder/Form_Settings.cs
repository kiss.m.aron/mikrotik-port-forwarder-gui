﻿using Kisem.Debug;
using System;
using System.IO;
using System.Windows.Forms;

namespace MikroTikPortForwarder
{
    public partial class Form_Settings : Form
    {
        public Form_Settings()
        {
            InitializeComponent();

            comboBox_Language.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach (var lang in Settings.Languages)
            {
                comboBox_Language.Items.Add(lang);
            }
            comboBox_Language.SelectedItem = Settings.Language;

            comboBox_Language.SelectionChangeCommitted += ComboBox_Language_SelectionChangeCommitted;
            
            Settings.Translate(this);

            textBox_PrefRouterIP.Text = Settings.Router.IP;
            textBox_PrefRouterUsername.Text = Settings.Router.Username;
            textBox_PrefRouterPassword.Text = Settings.Router.Password;

            Forms.list.Add(this);
        }


        private void ComboBox_Language_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Settings.Language = (sender as ComboBox).Text;
        }

        private void button_PrefSave_Click(object sender, EventArgs e)
        {
            Settings.Router.IP = textBox_PrefRouterIP.Text;
            Settings.Router.Username = textBox_PrefRouterUsername.Text;
            Settings.Router.Password = textBox_PrefRouterPassword.Text;
            Settings.Save();

            Forms.Update();
            Forms.list.Remove(this);
            Close();
        }
    }
}
